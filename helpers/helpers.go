package helpers

import (
	"context"
	"errors"
	config "etiennepericat/simple-todo/server/config/database"
	"etiennepericat/simple-todo/server/models"
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson"
)

// AuthenticateUserWithToken return true if the token is valid, false otherwise
func AuthenticateUserWithToken(r *http.Request) (bool, *models.User, error) {

	tokenString := r.Header.Get("Authorization")
	tokenString = strings.Split(tokenString, "Bearer ")[1]

	// retrieve a token object
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method")
		}
		return []byte("secret"), nil
	})

	if err != nil {
		return false, nil, errors.New("User authentication failed")
	}

	if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {

		// Look for the user
		collection := config.GetUsers()
		var user models.User
		err = collection.FindOne(context.TODO(), bson.D{{"identifier", token.Claims.(jwt.MapClaims)["identifier"]}}).Decode(&user)
		return true, &user, nil
	} else {
		return false, nil, errors.New("User authentication failed")
	}
}
