package logger

import (
	"log"
)
// LogError log an error in the system
func LogError(message string, err error) {
	
	if err != nil {
		log.Println("## __ Simple todo server ERROR : " + message + " __##")
		log.Println(err.Error())
	}
}

// LogInfo log an info in the system
func LogInfo(message string) {
	log.Println("## __ INFO : " + message + " __##")
}