package main

import (
	"etiennepericat/simple-todo/server/logger"
	"log"
	"net/http"

	routerconf "etiennepericat/simple-todo/server/config/http"
)

func main() {
	log.Println("-- Init Simple Todo Server --")
	router := routerconf.InitializeRouter()
	err := http.ListenAndServe(":8081", router)

	if err != nil {
		logger.LogError("error serving http server", err)
	}
}
